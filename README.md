# TM-Vagrant

[Vagrant](vagrantup.com) helps setting development environment. To use this environment, install prerequisites and setup box.

This Vagrant installs and configure :

* Essentials
	* git
	* vim
	* curl
	* zsh
* Maven, gradle
* JDK
* Tomcat 7
* Solr 4.5

## Setup

In order to use [Vagrant](vagrantup.com), you must have [VirtualBox](https://www.virtualbox.org/) installed. Afterwards, install Vagrant from the [download page](http://downloads.vagrantup.com/).

To use VirtualBox Guest Additions, install vagrant plugin: `vagrant plugin install vagrant-vbguest` (required to use shared folders).

Execute `git submodule init` and `git submodule update` to get Puppet modules latest version. 

## Use

Once Vagrant is installed, simple use `vagrant up` in the console. It will setup the virtual machine with desired box and configuration. If you update `Vagrantfile`, you can update the virtual machine accordingly by running `vagrant provision` (it has to be running).

The virtual machine is headless, to use the graphical interface uncomment `v.gui = true` in `Vagrantfile`.