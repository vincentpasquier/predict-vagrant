#
#
#

# Tomcat source and version
$tomcat_mirror  = "http://archive.apache.org/dist/tomcat"
$tomcat_version = "7.0.42"

$sesame_mirror  = "http://downloads.sourceforge.net/project/sesame/Sesame%202"
$sesame_version = "2.7.7"

$solr_mirror    = "http://artfiles.org/apache.org/lucene/solr"
$solr_version   = "4.5.1"

# Shell
$oh_my_zsh_git = "https://bitbucket.org/vincentpasquier/oh-my-zsh.git"
$sh_user       = "vagrant"
$zsh_path      = "/bin/zsh"

# Folders
$download_folder = "/home/${sh_user}/downloads"
$install_folder  = "/home/${sh_user}/install"
$solr_home			 = "${install_folder}/solr/home"

# Do not modify
# Extracts major tomcat version (\1 specifies token)
$tomcat_major    = regsubst($tomcat_version, '^(\d+)\.(\d+)\.(\d+)$', '\1')
$tomcat_filename = "apache-tomcat-${tomcat_version}.tar.gz"
$tomcat_url      = "${tomcat_mirror}/tomcat-${tomcat_major}/v${tomcat_version}/bin/${$tomcat_filename}"
$sesame_filename = "openrdf-sesame-${sesame_version}-sdk.tar.gz"
$sesame_url      = "${sesame_mirror}/${sesame_version}/${sesame_filename}"
$solr_filename   = "solr-${solr_version}.tgz"
$solr_url        = "${solr_mirror}/${solr_version}/${solr_filename}"

# https://github.com/puppetlabs/puppetlabs-apt
include apt

# https://github.com/puppetlabs/puppetlabs-git
include git

# Class: folders
#
#
class folders {
	# resources

	file { [ $download_folder, $install_folder ]:
		ensure => directory,
		owner  => $sh_user
	}
}

class system {

	file { "/etc/resolvconf/resolv.conf.d/tail":
		ensure    => file,
		content   => "nameserver 8.8.8.8
nameserver 8.8.4.4"
	}

	# Execute apt-get update
	exec { "update":
		path    => "/bin:/usr/bin",
		command => "service network-manager restart; apt-get update"
	}

	# Requires vim, curl and zsh
	package { ["vim", "curl", "zsh", "tmux"]:
		ensure => latest
	}

	user { "${sh_user}":
		ensure => present,
		comment => "Tomcat User",
		home => "/home/${sh_user}",
		shell => "/bin/zsh",
	}

	# Change shell for specified user
	exec { "chsh -s $zsh_path $sh_user":
		path    => "/bin:/usr/bin",
		unless  => "grep -E '^${sh_user}.+:${zsh_path}$' /etc/passwd",
		require => Package["zsh"]
	}

	# Clones oh-my-zsh from specified git
	exec { "clone_oh_my_zsh":
		path    => "/bin:/usr/bin",
		cwd     => "/home/${sh_user}",
		user    => $sh_user,
		command => "git clone ${oh_my_zsh_git} /home/${sh_user}/.oh-my-zsh",
		creates => "/home/${sh_user}/.oh-my-zsh",
		require => [Package["git", "zsh", "curl"]]
	}

	# Copies template to .zshrc
	exec { "copy-zshrc":
		path    => "/bin:/usr/bin",
		cwd     => "/home/${sh_user}",
		user    => $sh_user,
		command => "cp .oh-my-zsh/templates/zshrc.zsh-template .zshrc",
		unless  => "ls .zshrc",
		require => Exec["clone_oh_my_zsh"],
	}
}

# Requires maven
class mvn {
	package { ["maven", "gradle"]:
		ensure => latest,
	}
}

class tomcat {

	package { "supervisor":
		ensure  => latest,
	}

	exec { "download_tomcat":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "wget '${tomcat_url}'",
		unless  => "ls ${tomcat_filename}",
		notify  => Exec["extract_tomcat"]
	}

	exec { "extract_tomcat":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "mkdir tomcat; tar -xzf ${tomcat_filename} -C tomcat; mv tomcat/apache* ${install_folder}/tomcat; rm -rf tomcat",
		unless  => "ls ${install_folder}/tomcat"
	}

}

class solr {

	file { "${solr_home}":
		ensure  => directory,
		owner		=> $sh_user
	}

	exec { "download_solr":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "wget '${solr_url}'",
		unless  => "ls ${solr_filename}",
		notify  => Exec["extract_solr"]
	}

	exec { "extract_solr":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "mkdir solr; tar -xzf ${solr_filename} -C solr; mv solr/solr* ${install_folder}/solr; rm -rf solr",
		unless  => "ls ${install_folder}/solr",
		notify  => Exec["deploy_solr"]
	}

	exec { "deploy_solr":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${install_folder}",
		command => "cp solr/dist/solr-4.5.1.war tomcat/webapps;  cp solr/example/lib/ext/*.jar tomcat/lib; cp -R solr/example/solr/* ${solr_home}",
		unless  => "ls ${install_folder}/tomcat/webapps/solr-${solr_version}"
	}
}

class sesame {
	exec { "download_sesame":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "wget '${sesame_url}'",
		unless  => "ls ${sesame_filename}",
		notify  => Exec["extract_sesame"]
	}

	exec { "extract_sesame":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${download_folder}",
		command => "mkdir sesame; tar -xzf ${sesame_filename} -C sesame; mv sesame/openrdf* ${install_folder}/sesame; rm -rf sesame",
		unless  => "ls ${install_folder}/sesame",
		notify  => Exec["deploy_sesame"]
	}

	exec { "deploy_sesame":
		path 		=> "/bin:/usr/bin",
		user    => $sh_user,
		cwd     => "${install_folder}",
		command => "cp sesame/war/*.war tomcat/webapps",
		unless  => "ls ${install_folder}/tomcat/webapps/openrdf-*.war"
	}

}

class supervisor {

	file { "/etc/supervisor/conf.d/tomcat.conf":
		ensure    => present,
		content   => "[program:tomcat]
command=${install_folder}/tomcat/bin/catalina.sh run
directory=${install_folder}/tomcat/bin
autostart=no
user=vagrant
stopsignal=QUIT
environment=JAVA_OPTS='-Dsolr.solr.home=${solr_home}'",
		require   => [ Package["supervisor"] ],
		notify    => Exec["update_supervisor"],
	}

	exec { "update_supervisor":
		path 				=> "/bin:/usr/bin",
		command     => "sudo supervisorctl update",
		refreshonly => true,
	}

}

# Runs system
include folders
include system

# https://github.com/puppetlabs/puppetlabs-java
include java
include mvn
include tomcat
include solr
include sesame
include supervisor